var express = require('express');
var Queue = require('bull');
var QueueMQ = require('bullmq');
var createBullBoard = require('@bull-board/api').createBullBoard;
var BullAdapter = require('@bull-board/api/bullAdapter').BullAdapter;
var BullMQAdapter = require('@bull-board/api/bullMQAdapter').BullMQAdapter;
var ExpressAdapter = require('@bull-board/express').ExpressAdapter;
var someQueue = new Queue('myqueue', {
    redis: { host: "172.19.0.2", port: 6379 },
}); // if you have a special connection to redis.
// const someOtherQueue = new Queue('myqueue'), {
//   redis: { host: "172.19.0.3", port: 6379, password: "xasdf"},
// };
// const queueMQ = new QueueMQ('myqueue');
var serverAdapter = new ExpressAdapter();
serverAdapter.setBasePath('/admin/queues');
var _a = createBullBoard({
    queues: [new BullAdapter(someQueue) /*, new BullAdapter(someOtherQueue)*/],
    serverAdapter: serverAdapter,
}), addQueue = _a.addQueue, removeQueue = _a.removeQueue, setQueues = _a.setQueues, replaceQueues = _a.replaceQueues;
var app = express();
app.use('/admin/queues', serverAdapter.getRouter());
// other configurations of your server
app.listen(3000, function () {
    console.log('Running on 3000...');
    console.log('For the UI, open http://localhost:3000/admin/queues');
    console.log('Make sure Redis is running on port 6379 by default');
});
