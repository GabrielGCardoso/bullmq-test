// this is the producer of jobs

import { Queue, Worker } from 'bullmq';
// Create a new connection in every instance
const myQueue = new Queue('myqueue', {
  connection: {
    host: "172.19.0.2",
    port: 6379
  }
});


const sleep = async (ms: number) => new Promise((res, rej) => setTimeout(res, ms))

async function addJobs() {
  try {

    // the worker and the job tries to reconnect automatically until redis is available again
    await myQueue.add(
      'myJobName',
      { jobInfo: 'bar', foo: 'bar', startTime: new Date() },
      {
        jobId: 'bar',
        delay: 500,                       // delay em ms
        // removeOnComplete: true,
        // removeOnFail: true,               // if its okay to remove on failure
        repeat: {
          pattern: '10 * * * * *',        // in case we want to create a cron job
        },
        attempts: 5,                      // number of attempts
        backoff: {
          type: 'exponential',            // exponential or fixed
          delay: 1000                     // 2 ^ (attempts - 1) * delay
        }
      }
    );
    console.log("added bar")

    await myQueue.add('myJobName', { jobInfo: 'foo', qux: 'baz' }, {
      // removeOnComplete: true,
      // removeOnFail: true,               // it only delets after retrieving the job for the number of attempts
      attempts: 5,                      // number of attempts
      backoff: {
        type: 'exponential',            // exponential or fixed
        delay: 1000                     // 2 ^ (attempts - 1) * delay
      }
    });
    console.log("added baz")

  } catch (e) {
    console.log("e", e);
  }
}

(async () => {
    await myQueue.add(
      'myJobName',
      { jobId: 'zig', foo: 'zig', startTime: new Date() },
      {
        jobId: 'zig',
        delay: 500,                       // delay em ms
        // removeOnComplete: true,
        // removeOnFail: true,               // if its okay to remove on failure
        repeat: {
          pattern: '10 * * * * *',        // in case we want to create a cron job
        },
        attempts: 5,                      // number of attempts
        backoff: {
          type: 'exponential',            // exponential or fixed
          delay: 1000                     // 2 ^ (attempts - 1) * delay
        }
      }
    );
  for (let i = 0; i < 3; i++) { 
    await myQueue.add(
      'myJobName',
      { jobId: 'zig', foo: 'zig', startTime: new Date() },
      {
        jobId: 'zig',
        delay: 500,                       // delay em ms
        // removeOnComplete: true,
        // removeOnFail: true,               // if its okay to remove on failure
        repeat: {
          pattern: '10 * * * * *',        // in case we want to create a cron job
        },
        attempts: 5,                      // number of attempts
        backoff: {
          type: 'exponential',            // exponential or fixed
          delay: 1000                     // 2 ^ (attempts - 1) * delay
        }
      }
    );
    // it only enqueues 3 jobs as the delay to execute is greater than the sleep time
    await sleep(100);
    addJobs()
  }
})()

// it awaits the end from the current job 
const gracefulShutdown = async () => {
  await myQueue.close();
  console.log("graceful shutdown");
  process.exit(0);
}

// handle graceful shutdown
process.on('SIGTERM', gracefulShutdown);
process.on('SIGINT', gracefulShutdown);