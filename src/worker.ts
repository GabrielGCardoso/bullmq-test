// this is the consumer of jobs
import { Worker } from 'bullmq';

import { QueueEvents } from 'bullmq';

const REDIS_HOST = "172.19.0.2";
const REDIS_PORT = 6379;
const queueEvents = new QueueEvents('myqueue', {
    // concurrency: 2, // allow processing more than one job at a time
    connection: {
        host: REDIS_HOST,
        port: REDIS_PORT
    }
});

queueEvents.on('completed', ({ jobId, returnvalue }) => {
    console.log("completed", jobId, returnvalue);
});

queueEvents.on('failed', ({ jobId, failedReason }) => {
    // jobId received a progress event
    console.log("failed", jobId, failedReason);
});

queueEvents.on('progress', ({ jobId, data }) => {
    console.log('progress', jobId, data)
    // jobId received a progress event
});

const sleep = async (ms: number) => new Promise((res, rej) => setTimeout(res, ms))

const myWorker = new Worker('myqueue',
    async (job: any) => {
        if(Math.random() > 0.25) throw new Error('A random error occurred') // adding radom error to check retrivies

        job.log("waiting")
        job.updateProgress(1)

        await sleep(1000)

        job.log("finishied")
        job.updateProgress(100)

        console.log("job", job.id, job.data)
    }, {
    // concurrency: 2, // allow processing more than one job at a time
    connection: {
        host: REDIS_HOST,
        port: REDIS_PORT
    }
});

const shutdown = async () => {
    await myWorker.close();
    console.log("graceful shutdown");
    process.exit(0);
}

// handle graceful shutdown
process.on('SIGTERM', shutdown);
process.on('SIGINT', shutdown);